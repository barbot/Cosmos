#include "markingImpl.hpp"

const int reducePL_RE_P0 = 0;
const int reducePL_RE_AP = 1;

void REHandling::print_state(const vector<int> &vect) {
  cerr << "RE_P0 = " << vect[reducePL_RE_P0] << endl;
  cerr << "RE_AP = " << vect[reducePL_RE_AP] << endl;
}
bool REHandling::precondition(const abstractMarking &Marking) {
  return true; //Marking.P->_PL_Stack.card() >= threshold;
}

void REHandling::lumpingFun(const abstractMarking &Marking, vector<int> &vect) {
  exit(1);
}

double REHandling::direct_computation(const abstractMarking &Marking) {
  // return -1;
  double n = (double)Marking.P->_PL_Stack.card();
  if( n<= threshold) return 1.0;
  return pow((1 - p) / p, n - threshold);
}



Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.05
#6608 transition taken, deciveness_int :[0.00712934,0.0570941]
#Time for simulation:	0.004695596000000s
#Total Memory used:	240.2422 MB
#Cosmos tandem.grml tandem.lha --const "N=5" --width 0.05 --batch 100 
#|< 0          -[ 2.5062395e-05 < 0.005      > 0.036559131 ]- 1          >| width=0.03653407 level=0.99000000
#Total paths:	200
#Time for simulation:	0.001904581000000s


Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.02 
#85945 transition taken, deciveness_int :[0.00814326,0.0281308]
#Time for simulation:	0.033407565000000s
#Total Memory used:	240.2422 MB
#Cosmos tandem.grml tandem.lha --const "N=5" --width 0.02 --batch 100 
#|< 0          -[ 0.0011225238 < 0.0066666667 > 0.020840309 ]- 1          >| width=0.01971779 level=0.99000000
#Total paths:	600
#Time for simulation:	0.002163253000000s


Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.01
#898101 transition taken, deciveness_int :[0.00855719,0.0185569]
#Time for simulation:	0.349877491000000s
#Total Memory used:	240.2422 MB
#Cosmos tandem.grml tandem.lha --const "N=5" --width 0.01 --batch 100 
#|< 0          -[ 0.0027945496 < 0.0065     > 0.012708538 ]- 1          >| width=0.00991399 level=0.99000000
#Total paths:	2000
#Time for simulation:	0.003439120000000s


Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.005 
#9070366 transition taken, deciveness_int :[0.00875623,0.0137562]
#Time for simulation:	3.702119330000000s
#Total Memory used:	317.9102 MB
#Cosmos tandem.grml tandem.lha --const "N=5" --width 0.005 --batch 100 
#|< 0          -[ 0.0070601914 < 0.0093203883 > 0.012045834 ]- 1          >| width=0.00498564 level=0.99000000
#Total paths:	10300
#Time for simulation:	0.010307307000000s

Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.002 
#118071151 transition taken, deciveness_int :[0.00885996,0.01086]
#Time for simulation:	45.085610357000000s
#Total Memory used:	3987.9766 MB
#Cosmos tandem.grml tandem.lha --const "N=5" --width 0.002 --batch 100 
#|< 0          -[ 0.0083651666 < 0.0093269231 > 0.010364315 ]- 1          >| width=0.00199915 level=0.99000000
#Total paths:	62400
#Time for simulation:	0.051563426000000s

#Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.00175
#124390038 transition taken, deciveness_int :[0.00886405,0.0106141]
#Time for simulation:	47.282342174999997s
#Total Memory used:	4194.6680 MB

#Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.0017
#125847686 transition taken, deciveness_int :[0.00886487,0.0105649]
#Time for simulation:	48.554935438000001s
#Total Memory used:	4242.3984 MB

Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.0015
#Out of memory > 20GB

Cosmos tandem.grml tandem.lha --state-space --const "N=5" --width 0.0001


Cosmos tandem.grml tandem.lha --const "N=5" --width 0.001
#|< 0.00000000 -[ 0.00851009 < 0.00900000 > 0.00950957 ]- 1.00000000 >| width=0.00099947 level=0.99000000
#Total paths:	239000
#Time for simulation:	0.187376542000000s

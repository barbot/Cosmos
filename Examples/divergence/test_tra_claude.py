"""heavely refactor by Claude and chatGPT from file test_tra.py"""

import math
import sys


def create_arc(arc_id, source, target, valuation, is_token=False):
    """
    Generate GRML XML for an arc with given parameters.
    
    Args:
        arc_id: The unique id for the arc.
        source: The source node id.
        target: The target node id.
        valuation: 
            If is_token is False, a value to be wrapped inside
            <attribute name="expr"><attribute name="numValue">…</attribute></attribute>.
            If is_token is True, the full XML fragment (as a string) for the token attribute.
        is_token: Whether to treat the valuation as a token (i.e. no extra wrapping).
    
    Returns:
        A string containing the XML representation of the arc.
    """
    xml = []
    xml.append(f'<arc id="{arc_id}" arcType="arc" source="{source}" target="{target}">')
    if is_token:
        xml.append(f'\t<attribute name="valuation"><attribute name="token">')
        xml.append(f'\t\t<attribute name="occurs"><attribute name="intValue">1</attribute></attribute>')
        xml.append(f'\t\t<attribute name="tokenProfile">{valuation}</attribute>')
        xml.append(f'\t</attribute></attribute>')
    else:
        xml.append(f'\t<attribute name="valuation"><attribute name="expr"><attribute name="numValue">{valuation}</attribute></attribute></attribute>')
    xml.append('</arc>')
    return "\n".join(xml) + "\n"


def create_transition(node_id, name, distribution, param, priority, weight, extra=""):
    """
    Generate GRML XML for a transition node.
    
    Args:
        node_id: The unique id for the transition node.
        name: The name attribute.
        distribution: The distribution type (e.g. EXPONENTIAL).
        param: The parameter value for the distribution.
        priority: The priority value.
        weight: The weight value.
        extra: Any additional XML fragment to append.
    
    Returns:
        A string containing the XML representation of the transition node.
    """
    xml = []
    xml.append(f'<node id="{node_id}" nodeType="transition">')
    xml.append(f'\t<attribute name="name">{name}</attribute>')
    xml.append('\t<attribute name="distribution">')
    xml.append(f'\t\t<attribute name="type">{distribution}</attribute>')
    xml.append('\t\t<attribute name="param">\n\t\t\t<attribute name="number">0</attribute>')
    xml.append(f'\t\t\t<attribute name="expr">{param}</attribute>')
    xml.append('\t</attribute></attribute>')
    xml.append(f'\t<attribute name="priority"><attribute name="expr"><attribute name="numValue">{priority}</attribute></attribute></attribute>')
    xml.append(f'\t<attribute name="weight"><attribute name="expr"><attribute name="numValue">{weight}</attribute></attribute></attribute>')
    
    if extra:
        xml.append(extra)
    xml.append('</node>')
    return "\n".join(xml) + "\n"


def create_place(node_id, name, domain=None, marking_expr=None, marking_token=None, extra=""):
    """
    Generate GRML XML for a place node.
    
    Args:
        node_id: The unique id for the place node.
        name: The name attribute.
        domain: (Optional) Domain type string to be used inside a domain attribute.
        marking_expr: (Optional) A value to be wrapped as an expression for marking.
        marking_token: (Optional) A full XML fragment string for token marking.
        extra: (Optional) Additional XML fragment to append.
        
    Returns:
        A string containing the XML representation of the place node.
    """
    xml = []
    xml.append(f'<node id="{node_id}" nodeType="place">')
    xml.append(f'\t<attribute name="name">{name}</attribute>')
    if domain is not None:
        xml.append(f'\t<attribute name="domain"><attribute name="type">{domain}</attribute></attribute>')
    if marking_expr is not None:
        xml.append(f'\t<attribute name="marking"><attribute name="expr"><attribute name="numValue">{marking_expr}</attribute></attribute></attribute>')
    elif marking_token is not None:
        xml.append(f'\t<attribute name="marking"><attribute name="token">{marking_token}</attribute></attribute>')
    if extra:
        xml.append(extra)
    xml.append('</node>')
    return "\n".join(xml) + "\n"


def poly(n, coefficients):
    """Calculate the value of a polynomial with given coefficients at point n."""
    k = 1
    result = 0
    for coef in reversed(coefficients):
        result += k * coef
        k *= n
    return result


def main():
    """Main function to process the input file and generate GRML output."""
    if len(sys.argv) < 2:
        print("Usage: python test_tra.py <input_file>")
        return

    theta = 0.01
    minimum = 1e6

    # Open input and output files
    with open(sys.argv[1], "r") as input_file:
        lines = input_file.readlines()

    with open("output.grml", "w") as out:
        # Initialize variables
        node_id = 0
        dw = {}  # Dictionary to track weights
        transition_count = 0
        dc = {}  # Dictionary for channel nodes
        ds = {}  # Dictionary for state nodes
        di = {}  # Dictionary for input nodes
        dcap = {}  # Dictionary for capacity nodes
        polynomials = []
        myset = {"$"}
        capacity = 1e6
        channel_count = 0
        counter = -1
        sum_weight = 0

        # Write XML header
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        out.write('<model formalismUrl="http://formalisms.cosyverif.org/sptgd-net.fml" xmlns="http://cosyverif.org/ns/model">\n')

        # First pass: collect information
        for line in lines:
            s = line.strip().split('#')[0]
            tokens = s.split()
            if not tokens:
                continue

            if tokens[0] == "W":
                coeffs = [int(tokens[j]) for j in range(len(tokens) - 1, 0, -1)]
                polynomials.append(coeffs)
            elif tokens[0] == "T":
                sum_weight += int(tokens[5])
                myset.add(tokens[2][3])
                myset.add(tokens[3][3])
            elif tokens[0] == "Channel" and tokens[3] != "inf":
                capacity = min(int(tokens[3]), capacity)

        # Remove special characters from client set
        myset.discard("$")
        myset.discard("-")

        # Sort polynomials
        polynomials.sort()

        # Calculate minimum number of clients
        l_bound = 0
        r_bound = minimum
        clients = len(myset) or 1  # Ensure at least 1 client

        while r_bound - l_bound > 1:
            mid = (l_bound + r_bound) // 2
            if poly(mid, polynomials[0]) < sum_weight:
                l_bound = mid
            else:
                r_bound = mid

        minimum = min(minimum, r_bound)
        maxim = 1e6
        answer = 0

        # Calculate maximum clients
        while True:
            answer = maxim
            pv = poly(minimum, polynomials[0])
            epsy = (pv - sum_weight) / (pv + sum_weight)
            gamma = math.exp((epsy / (epsy + 1)) ** 2)

            if gamma == 1:
                break

            x = -(math.log((math.sqrt(gamma) - 1) * theta)) / math.log(gamma)
            maxim = x + minimum
            minimum += 1

            if maxim > answer:
                break

        print(f"x = {math.ceil(answer + 1 - minimum)}, v = {math.ceil(minimum - 1)}")
        
        # Write declarations
        declaration = (
            f'<attribute name="declaration">\n'
            f'<attribute name="constants"><attribute name="intConsts">'
            f'<attribute name="intConst"><attribute name="name">NbClient</attribute><attribute name="expr">'
            f'<attribute name="numValue">{clients}</attribute></attribute></attribute>'
            f'<attribute name="intConst"><attribute name="name">NbPosition</attribute><attribute name="expr">'
            f'<attribute name="numValue">{1 + math.ceil(answer)}</attribute></attribute></attribute>'
            f'</attribute></attribute>\n'
            f'<attribute name="classDeclaration"><attribute name="name">client</attribute>'
            f'<attribute name="classType"><attribute name="classEnum">'
        )
        out.write(declaration)

        # Second pass: process instructions
        count_poly=0
        for line in lines:
            counter += 1
            s = line.strip().split('#')[0]
            tokens = s.split()
            if not tokens:
                continue
            
            if tokens[0] == "C":
                channel_count = len(tokens) - 1

                for j in range(1, len(tokens)):
                    if j - 1 != 0:
                        # Create channel place using helper function
                        channel_xml = create_place(node_id, f"C{j-1}", domain=f"CP{j-1}", marking_expr="0")
                        out.write(channel_xml)
                        dc[j-1] = node_id
                        node_id += 1

                        # Create input place using helper function
                        token_marking = (
                            '<attribute name="token"><attribute name="occurs"><attribute name="intValue">1</attribute></attribute>'
                            '<attribute name="tokenProfile"><attribute name="expr"><attribute name="enumConst"><attribute name="type">'
                            f'position{j-1}</attribute><attribute name="enumValue">n0</attribute></attribute></attribute></attribute></attribute>'
                        )
                        input_xml = create_place(node_id, f"In{j-1}", domain=f"position{j-1}", marking_token=token_marking)
                        out.write(input_xml)
                        di[j-1] = node_id
                        node_id += 1
                    else:
                        # Write client enum values (remains unchanged)
                        for i in range(clients):
                            out.write(f'<attribute name="enumValue">x{i+1}</attribute>')
                        out.write('</attribute></attribute><attribute name="circular">false</attribute></attribute>')

                        # Create position declarations (unchanged)
                        for i in range(1, len(tokens)-1):
                            position_decl = (
                                f'<attribute name="classDeclaration"><attribute name="name">position{i}</attribute>'
                                f'<attribute name="classType"><attribute name="classIntInterval"><attribute name="lowerBound">1</attribute>'
                                f'<attribute name="higherBound"><attribute name="name">NbPosition</attribute></attribute></attribute></attribute>'
                                f'<attribute name="circular">true</attribute></attribute>'
                            )
                            out.write(position_decl)
                            domain_decl = (
                                f'<attribute name="domainDeclaration"><attribute name="name">CP{i}</attribute>'
                                f'<attribute name="domainType"><attribute name="cartesianProduct"><attribute name="type">client</attribute>'
                                f'<attribute name="type">position{i}</attribute></attribute></attribute></attribute>'
                            )
                            out.write(domain_decl)

                        # Create variable declarations (unchanged)
                        for i in range(1, len(tokens)-1):
                            var_decl = (
                                f'<attribute name="variableDeclaration"><attribute name="name">p{i}</attribute>'
                                f'<attribute name="type">position{i}</attribute></attribute>'
                            )
                            out.write(var_decl)
                        out.write('<attribute name="variableDeclaration"><attribute name="name">x</attribute><attribute name="type">client</attribute></attribute>\n')
                        out.write('</attribute>\n')

                        # Create count place using helper function
                        count_place = create_place(node_id, "Count", marking_expr="1")
                        out.write(count_place)
                        node_id += 1

                        # Create C0 place using helper function
                        c0_place = create_place(node_id, f"C{j-1}", marking_expr="0")
                        out.write(c0_place)
                        dc[j-1] = node_id
                        node_id += 1

            elif tokens[0] == "S":
                # Create capacity places
                for j in range(2, channel_count + 1):
                    capacity_place = create_place(node_id, f"Capacity{j-1}", marking_expr=str(capacity))
                    out.write(capacity_place)
                    dcap[j-1] = node_id
                    node_id += 1

                # Create state places
                for j in range(1, len(tokens)):
                    dw[j-1] = 0
                    state_place = create_place(node_id, f"S{j-1}", marking_expr=str(1 if j == 1 else 0))
                    out.write(state_place)
                    ds[j-1] = node_id
                    node_id += 1

            elif tokens[0] == "T":
                dw[int(tokens[1])] += int(tokens[5])
                trans_id = node_id

                # Validate transitions
                if tokens[3][3] == "$":
                    raise ValueError("An unidentified Client Cannot enter into any other Channel except the Input Channel")
                if tokens[3][1] == "0":
                    raise ValueError("A Client Cannot enter the Input Channel from any other channel")

                # Create transition node using helper function (without isHorizontal)
                if tokens[3][1] != "-":
                    name = f"T_Q{tokens[2][1]}_Q{tokens[3][1]}_{transition_count}"
                else:
                    name = f"DT_Q{tokens[2][1]}_Q{'D'}_{transition_count}"
                trans_xml = create_transition(node_id, name, "EXPONENTIAL", tokens[5], "1", "1")
                out.write(trans_xml)
                node_id += 1

                # Create arcs using the unified function
                out.write(create_arc(node_id, ds[int(tokens[1])], trans_id, 1))
                node_id += 1
                out.write(create_arc(node_id, trans_id, ds[int(tokens[4])], 1))
                node_id += 1

                # Handle special case for deletion
                if tokens[3][1] == "-":
                    out.write(create_arc(node_id, "0", trans_id, 1))
                    node_id += 1
                    out.write(create_arc(node_id, trans_id, "0", 0))
                    node_id += 1

                transition_count += 1

                # Handle channel arcs
                if tokens[2][1] != "0":
                    # Arc from channel to transition (token arc)
                    arc5_val = (
                        '<attribute name="expr"><attribute name="colorConst"><attribute name="type">client</attribute>'
                        f'<attribute name="name">x{ord(tokens[2][3]) - 96}</attribute></attribute></attribute>'
                        '<attribute name="expr"><attribute name="colorConst"><attribute name="type">position'
                        f'{int(tokens[2][1])}</attribute><attribute name="enumConst">n0</attribute></attribute></attribute>'
                    )
                    out.write(create_arc(node_id, dc[int(tokens[2][1])], trans_id, arc5_val, is_token=True))
                    node_id += 1
                    # Arc from transition to capacity
                    out.write(create_arc(node_id, trans_id, dcap[int(tokens[2][1])], 1))
                    node_id += 1
                else:
                    # Arc from channel 0 to transition
                    out.write(create_arc(node_id, dc[int(tokens[2][1])], trans_id, 1))
                    node_id += 1

                # Handle destination channel if not deletion
                if tokens[3][1] != "-":
                    # Arc from input to transition (token arc)
                    arc8_val = (
                        '<attribute name="expr"><attribute name="name">p'
                        f'{int(tokens[3][1])}</attribute></attribute>'
                    )
                    out.write(create_arc(node_id, di[int(tokens[3][1])], trans_id, arc8_val, is_token=True))
                    node_id += 1

                    # Arc from transition to input (token arc)
                    arc9_val = (
                        '<attribute name="expr"><attribute name="function"><attribute name="++">'
                        f'<attribute name="name">p{int(tokens[3][1])}</attribute><attribute name="intValue">1</attribute>'
                        '</attribute></attribute></attribute>'
                    )
                    out.write(create_arc(node_id, trans_id, di[int(tokens[3][1])], arc9_val, is_token=True))
                    node_id += 1

                    # Arc from transition to channel (token arc)
                    arc10_val = (
                        '<attribute name="expr"><attribute name="colorConst"><attribute name="type">client</attribute>'
                        f'<attribute name="name">x{ord(tokens[3][3]) - 96}</attribute></attribute></attribute>'
                        '<attribute name="expr"><attribute name="name">p'
                        f'{int(tokens[3][1])}</attribute></attribute>'
                    )
                    out.write(create_arc(node_id, trans_id, dc[int(tokens[3][1])], arc10_val, is_token=True))
                    node_id += 1

                    # Arc from capacity to transition
                    out.write(create_arc(node_id, dcap[int(tokens[3][1])], trans_id, 1))
                    node_id += 1

            elif tokens[0] == "W":
                # Process polynomial function
                start = ""
                end = ""
                for k in range(len(tokens) - 2):
                    start += '\t<attribute name="function"><attribute name="+"> \n'
                for j in range(1, len(tokens)):
                    start2 = ""
                    end2 = ""
                    for k in range(j - 1):
                        start2 += '\t<attribute name="function"><attribute name="*"> \n'
                        end2 = ('\t<attribute name="function"><attribute name="-"><attribute name="name">Count</attribute>'
                                '<attribute name="numValue">1</attribute>\n\t</attribute></attribute></attribute></attribute> \n' + end2)
                    start2 += '\t<attribute name="numValue">' + tokens[j] + "</attribute>\n"
                    start += start2 + end2
                    if j > 1:
                        start += "\t</attribute></attribute> \n"

                # Create transition using helper function (without isHorizontal)
                trans_xml = create_transition(node_id, "InT" + str(transition_count), "EXPONENTIAL", start, "0", "1")
                trans_id = node_id
                transition_count += 1
                out.write(trans_xml)
                node_id += 1

                # Create arcs for W transitions using arc sets
                
                id_place_state = ds[count_poly]
                count_poly+=1
                
                arc_sets = [
                    (0, trans_id, 1),
                    (trans_id, 1, 1),
                    (trans_id, 0, 2),
                    (id_place_state, trans_id, 1),
                    (trans_id, id_place_state, 1)
                ]
                for idx, (src, tgt, val) in enumerate(arc_sets):
                    out.write(create_arc(node_id, src, tgt, val))
                    node_id += 1

        # Create additional places using helper function
        additional_places = [
            ('Cmax', clients),
            ('Wmax', math.ceil(answer)),
            ('N0', math.ceil(minimum - 1))
        ]
        for name, value in additional_places:
            place_xml = create_place(node_id, name, marking_expr=str(value))
            out.write(place_xml)
            node_id += 1

        # Write model closing tag
        out.write("</model>\n")


if __name__ == "__main__":
    main()
